class ForensicsSolver
  NORTH = 0
  EAST = 1
  SOUTH = 2
  WEST = 3
  ALL_DIRECTIONS = 4

  def initialize(directions)
    @directions = directions
    @x = 0
    @y = 0
    @absolute_direction = NORTH
  end

  def solve
    @directions.each do |step|
      move(step)
    end
    {x: @x, y: @y, absolute_direction: absolute_directions_symbol_names[@absolute_direction]}
  end

  private

  def absolute_directions_symbol_names
    {NORTH => :north, EAST => :east, SOUTH => :south, WEST => :west}
  end

  def move(step)
    turn(step)
    move_forward
  end

  def turn(step)
    return if step == :forward
    @absolute_direction = if step == :right
                            (@absolute_direction + 1) % ALL_DIRECTIONS
                          elsif step == :left
                            (@absolute_direction - 1) % ALL_DIRECTIONS
    end
  end

  def move_forward
    case @absolute_direction
    when NORTH
      @y += 1
    when SOUTH
      @y -= 1
    when WEST
      @x -= 1
    when EAST
      @x += 1
    end
  end
end
