require './spec/spec_helper.rb'

describe Api::ForensicsController, type: :controller do
  render_views

  describe '#directions' do
    before { get :directions, email: 'test@example.com', format: :json }

    it { expect(response).to be_success }

    it 'should return directions' do
      expect(assigns(:directions)).to be_kind_of(Array)
    end
  end

  describe '#location' do
    context 'correct guess' do
      let(:x) { 1 }
      let(:y) { 3 }
      before { get :location, email: 'test@example.com', format: :json, x: x, y: y }

      it { expect(response).to be_success }
      it { expect(assigns(:result)).to eq('found') }
    end

    context 'wrong guesses' do
      let(:x) { 1 }
      let(:y) { 4 }

      context 'wrong guesses <= 5' do
        before { 5.times { get :location, email: 'test@example.com', format: :json, x: x, y: y }}

        it { expect(response).to be_success }

        it { expect(assigns(:result)).to eq('not found') }
      end

      context 'wrong guesses > 5' do
        before { 6.times { get :location, email: 'test@example.com', format: :json, x: x, y: y }}

        it { expect(response).to be_success }

        it { expect(assigns(:result)).to eq('maximum guesses exceeded') }
      end
    end
  end
end
