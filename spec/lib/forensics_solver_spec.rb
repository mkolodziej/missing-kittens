require './spec/spec_helper.rb'

describe ForensicsSolver do

  describe 'coordinations for given directions' do
    subject { ForensicsSolver.new(directions).solve }

    context '1 step' do
      let(:directions) { [:forward] }
      it { is_expected.to eq({x: 0, y: 1, absolute_direction: :north}) }
    end

    context '2 step' do
      let(:directions) { [:forward, :forward] }
      it { is_expected.to eq({x: 0, y: 2, absolute_direction: :north}) }
    end

    context '3 step' do
      let(:directions) { [:forward, :forward, :right] }
      it { is_expected.to eq({x: 1, y: 2, absolute_direction: :east}) }
    end

    context '4 step' do
      let(:directions) { [:forward, :forward, :right, :right] }
      it { is_expected.to eq({x: 1, y: 1, absolute_direction: :south}) }
    end

    context '5 step' do
      let(:directions) { [:forward, :forward, :right, :right, :left] }
      it { is_expected.to eq({x: 2, y: 1, absolute_direction: :east}) }
    end

    context '6 step' do
      let(:directions) { [:forward, :forward, :right, :right, :left, :left] }
      it { is_expected.to eq({x: 2, y: 2, absolute_direction: :north}) }
    end
  end
end
