class Api::ForensicsController < ApplicationController
  include Api::ForensicsLocationHelper

  respond_to :json

  # Example: api/test@gmail.com/directions.json
  def directions
    @directions = DIRECTIONS
  end

  # Example: /api/test@gmail.com/location/1/4.json
  def location
    @result = validate_answer_and_respond
  end
end
