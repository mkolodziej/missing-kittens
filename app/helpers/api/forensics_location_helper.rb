module Api::ForensicsLocationHelper
  MAXIMUM_GUESSES = 5
  DIRECTIONS = [:forward, :right, :left, :forward]

  def validate_answer_and_respond
    increase_number_of_guesses

    if number_of_guesses_exceeded
      'maximum guesses exceeded'
    else
      if found
        reset_number_of_guesses
      end
      found ? 'found' : 'not found'
    end
  end

  private

  def found
    return @found if @found
    coordinates = ForensicsSolver.new(DIRECTIONS).solve
    @found = params[:x].to_i == coordinates[:x] && params[:y].to_i == coordinates[:y]
  end

  def number_of_guesses_exceeded
    number_of_guesses > MAXIMUM_GUESSES
  end

  def increase_number_of_guesses
    session[location_guesses] ||= 0
    session[location_guesses] += 1
  end

  def reset_number_of_guesses
    session[location_guesses] = 0
  end

  def location_guesses
    "#{params[:email]}_location_guesses"
  end

  def number_of_guesses
    session[location_guesses] ||= 0
  end

end
